from tkinter import filedialog
from tkinter import messagebox
from tkinter import *   #GUI
from random import randint as rand
import os
import re   #Regex

class GUI:
    def __init__(self, master):
        self.master = master
        master.title("Beautiful Code")
        self.nombreArchivo = ""
        #Vars
        self.eVar = StringVar()
        self.vIdentacion = IntVar()
        self.vComentarios = IntVar()
        self.vIdentacion.set(0)
        self.vComentarios.set(0)
        # Label
        self.lAntes = Label(master, text = "Antes")
        self.lDespues = Label(master, text = "Despues")
        # Boton
        self.btEnter = Button(master, text="Arreglar", command=self.llamarFixer)
        self.btMinimizar = Button(master, text="Minimizar", command=self.llamarMinimizer)
        self.btTips = Button(master, text="Tips", command=self.ventanaTips)
        self.btEscoger = Button(master, text="Seleccionar archivo", command=self.archivoSeleccionado)
        self.btOpciones = Button(master, text="Opciones", command=self.ventanaOpciones)
        #Entry
        self.eRuta = Entry(master, text=self.eVar, width=30)
        #TextBox
        self.tAntes = Text(master, height=10, width=40)
        self.tDespues = Text(master, height=10, width=40)
        #Grid
        self.btEscoger.grid(sticky="W",row=0, column=1)
        self.eRuta.grid(sticky="W",row=0, column=0)
        self.btEnter.grid(sticky="E",row=3, column=1)
        self.btMinimizar.grid(sticky="E",row=3, column=3)
        self.btOpciones.grid(sticky="E",row=3, column=0)
        self.btTips.grid(sticky="E",row=3, column=2)
        self.lAntes.grid(sticky="W",row=1, column=0, columnspan=2)
        self.lDespues.grid(sticky="W",row=1, column=2, columnspan=2)
        self.tAntes.grid(sticky="W",row=2, column=0)
        self.tDespues.grid(sticky="W",row=2, column=2)

        #Config
        self.eRuta.config(state='readonly')
        self.tAntes.configure(state='disabled')
        self.tDespues.configure(state='disabled')

    def ventanaTips(self):
        t = Toplevel()
        # TextBox
        self.tTip = Text(t, height=25, width=30)
        self.tTip.grid(sticky="W", row=0, column=0,  rowspan=2, columnspan=2)

        #Button
        self.bFIzquierda = Button(t, text=">", command= lambda: self.nuevoTip(self.tTip))
        self.bFIzquierda.grid(sticky="W", row=2, column=2,  rowspan=2, columnspan=2)
        self.nuevoTip(self.tTip)


    def nuevoTip(self, textBox):
        tips = [
            " Usa 4 (cuatro) espacios por indentación.\nPara código realmente antiguo que no quieras estropear, puedes "
            "continuar usando indentaciones de 8 (ocho) espacios.",

            "El paréntesis / corchete / llave que cierre una asignación debe estar "
            "alineado con el primer carácter que no sea un espacio en blanco",

            "¿Tabulaciones o espacios? "
            "Nunca mezcles tabulaciones y espacios.",

            "Limita todas las líneas a un máximo de 79 caracteres."
        ]
        textBox.configure(state='normal')
        textBox.delete('1.0', END)
        textBox.insert('end', tips[rand(1, (len(tips) - 1))])
        textBox.configure(state='disabled')

    def ventanaOpciones(self):
        t = Toplevel()
        # TextBox
        self.tIdent = Text(t, height=10, width=10)
        self.tComent = Text(t, height=10, width=10)

        #Button
        self.rIdenentacion1 = Radiobutton(t, text="Tipo A", variable=self.vIdentacion, value=0, command=lambda : self.actualizarTextOpcionesIdent(self.tIdent))
        self.rIdenentacion2 = Radiobutton(t, text="Tipo B", variable=self.vIdentacion, value=1, command=lambda : self.actualizarTextOpcionesIdent(self.tIdent))
        self.rComentario1 = Radiobutton(t, text="Comentar", variable=self.vComentarios, value=0, command=lambda : self.actualizarTextOpcionesComent(self.tComent))
        self.rComentario2 = Radiobutton(t, text="No Comentar", variable=self.vComentarios, value=1, command=lambda : self.actualizarTextOpcionesComent(self.tComent))

        #Grid
        self.tIdent.grid(sticky="W", row=0, column=0,  rowspan=2, columnspan=2)
        self.tComent.grid(sticky="W", row=2, column=0,  rowspan=2, columnspan=2)
        self.rIdenentacion1.grid(sticky="W", row=0, column=3)
        self.rIdenentacion2.grid(sticky="W", row=1, column=3)
        self.rComentario1.grid(sticky="W", row=2, column=3)
        self.rComentario2.grid(sticky="W", row=3, column=3)

        #Config
        self.actualizarTextOpcionesIdent(self.tIdent)
        self.actualizarTextOpcionesComent(self.tComent)
        self.tIdent.configure(state='disabled')
        self.tComent.configure(state='disabled')

    def actualizarTextOpcionesIdent(self, textBox):
        if self.vIdentacion.get() is 0:
            textBox.configure(state='normal')
            textBox.delete('1.0', END)
            textBox.insert('end', 'for(;;){\n\n}')
            textBox.configure(state='disabled')
        else:
            textBox.configure(state='normal')
            textBox.delete('1.0', END)
            textBox.insert('end', 'for(;;)\n{\n\n}')
            textBox.configure(state='disabled')

    def actualizarTextOpcionesComent(self, textBox):
        if self.vComentarios.get() is 0:
            textBox.configure(state='normal')
            textBox.delete('1.0', END)
            textBox.insert('end', "/*\n*\n*Función\n*\n*/\nint main(){")
            textBox.configure(state='disabled')
        else:
            textBox.configure(state='normal')
            textBox.delete('1.0', END)
            textBox.insert('end', 'int main(){')
            textBox.configure(state='disabled')

    def archivoSeleccionado(self):
        self.conseguirNombre()
        if(self.validarArchivo()):
            self.actualizarAntes()

    def actualizarAntes(self):
        max = 15
        count = 0
        self.tAntes.configure(state='normal')
        self.tAntes.delete('1.0', END)
        with open(self.nombreArchivo, "r") as f:
            for line in f:
                if count < max:
                    self.tAntes.insert('end',line)
                    count +=1
                else:
                    break
            self.tAntes.configure(state='disabled')

    def actualizarDespues(self):
        max = 15
        count = 0
        self.tDespues.configure(state='normal')
        self.tDespues.delete('1.0', END)
        with open(self.nombreArchivo, "r") as f:
            for line in f:
                if count < max:
                    self.tDespues.insert('end',line)
                    count +=1
                else:
                    break
            self.tDespues.configure(state='disabled')

    def conseguirNombre(self):
        self.nombreArchivo = filedialog.askopenfilename(initialdir="C:\\", title="Seleccionar archivo",
                                   filetypes=(("C", "*.c*"),("Java", "*.java"),
                                              ("Python", "*.py*")))
        self.eVar.set(self.nombreArchivo)

    def llamarFixer(self):
        if (self.validarArchivo()):
            Fixer(self.nombreArchivo,self.vIdentacion.get(),self.vComentarios.get())
            self.actualizarDespues()

    def llamarMinimizer(self):
        Minimizer(self.nombreArchivo)

    def validarArchivo(self):
        try:
            file = open(self.nombreArchivo, "r")
            return True
        except FileNotFoundError:
            messagebox.showerror("Error", "Error al abrir archivo")
            return False

class Minimizer:
    def __init__(self,  nombreArchivo):
        self.nombreArchivo = nombreArchivo
        self.nombreArchivoTemp = self.nombreArchivo + ".temp"
        self.extension = nombreArchivo[nombreArchivo.rfind(".") + 1:]
        self.nombreArchivoMin = self.nombreArchivo[:nombreArchivo.rfind(".")] + "min." + self.extension
        self.minimizar()
        os.remove(self.nombreArchivoTemp)

    def sobreescribir(self):
        with open(self.nombreArchivoTemp, "r") as f:
            with open(self.nombreArchivoMin, "w") as f1:
                for line in f:
                    f1.write(line)


    def minimizar(self):
        with open(self.nombreArchivo, 'r+') as file:    #Lectura de archivo original
            with open(self.nombreArchivoTemp, "w") as temp: #Escritura der archivo temporal
                for line in file:
                    temp.write(' '.join(line.split()))
        self.sobreescribir()


class Fixer:
    def __init__(self, nombreArchivo, tipoIdent, tipoComent):
        self.nombreArchivo = nombreArchivo
        self.nombreArchivoTemp = self.nombreArchivo + ".temp"
        self.extension = nombreArchivo[nombreArchivo.rfind(".") + 1:]
        self.tipoIdentacion = tipoIdent
        self.tipoComentario = tipoComent
        self.arreglarCodigo()
        os.remove(self.nombreArchivoTemp)

    def arreglarCodigo(self):
        self.crearTemp()
        self.mejorarIdentacion()
        if self.tipoComentario is 0:
            self.comentarFunciones()

    def crearTemp(self):
        file = open(self.nombreArchivoTemp, "w")


    def comentarFunciones(self):
        with open(self.nombreArchivo, 'r+') as file:  # Lectura de archivo original
            with open(self.nombreArchivoTemp, "w") as temp:  # Escritura der archivo temporal
                for line in file:
                    lineAEscribir = ""
                    if self.extension is 'c':
                        match = re.match(r'(int|char|char*|float|struct|double|void)\s\w+\(\w*\)', line)
                    elif self.extension is 'py':
                        match = re.match(r'def\s\w+\(\w*\)', line)
                    elif self.extension is 'java':
                        match = re.match(r'(public|private|protected)\s(static|final|abstract)?(int|char|char*|float|struct|double)\s\w+\(\w*\)', line)
                    if match:
                        if self.extension is 'c' or self.extension is 'java':
                            lineAEscribir += "/*\n*\n*Función\n*\n*/\n"
                        else:
                            lineAEscribir += "#\n#\n#Función\n#\n#\n"
                    lineAEscribir += line
                    temp.write(lineAEscribir)
        self.sobreescribir()



    def sobreescribir(self):
        with open(self.nombreArchivoTemp,"r") as f:
            with open(self.nombreArchivo,"w") as f1:
                for line in f:
                    f1.write(line)

    def mejorarIdentacion(self):
        with open(self.nombreArchivo, 'r+') as file:    #Lectura de archivo original
            with open(self.nombreArchivoTemp, "w") as temp: #Escritura der archivo temporal
                pila = []
                for line in file:
                    count = 0
                    bandera = 0
                    newLine = re.sub(r"\s+", " ", line)
                    print(newLine)
                    for char in newLine:
                        count += 1
                        if (char is '{'):
                            pila.append('{')
                            complemento = '\n' + (("\t" * (len(pila) - 1)) + "{\n" if self.tipoIdentacion is 1 else "") + ("\t" * len(pila))
                            chunk = newLine[bandera:count - self.tipoIdentacion] + complemento
                            bandera = count
                            print(chunk,bandera)
                            temp.write(chunk)
                        elif (char is '}'):
                            chunk = newLine[bandera:count - 1] + '\n' + ("\t" * (len(pila) - 1)) + '}'
                            bandera = count
                            temp.write(chunk)
                            print(chunk,bandera)
                            pila.pop()

        self.sobreescribir()
root = Tk()
my_gui = GUI(root)
root.mainloop()